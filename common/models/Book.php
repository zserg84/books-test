<?php

namespace common\models;

use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "book".
 *
 * @property integer $id
 * @property string $title
 * @property integer $year
 * @property string $description
 * @property string $isbn
 * @property string $image
 *
 * @property BookAuthor[] $bookAuthors
 * @property Author[] $authors
 */
class Book extends \yii\db\ActiveRecord
{
    public $img;

    protected $_imagePath;
    private $_authorNames;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'year', 'isbn'], 'required'],
            [['year'], 'integer'],
            [['description'], 'string'],
            [['title', 'isbn'], 'string', 'max' => 255],
            [['isbn'], 'unique', 'filter' => function($query){
                if(!$this->isNewRecord)
                    $query->andWhere(['<>', 'book.id', $this->id]);
            }],
            [['authorNames'], 'safe'],
            [['img'], 'file', 'mimeTypes'=> ['image/png', 'image/jpeg', 'image/gif'], 'wrongMimeType' => 'Недопустимый тип файла'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'year' => 'Год выпуска',
            'description' => 'Описание',
            'isbn' => 'Isbn',
            'image' => 'Обложка',
            'authorNames' => 'Авторы',
        ];
    }

    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($this->img){
                $this->img->name = uniqid() . '.' . $this->img->extension;
                $this->image = $this->img->name;
                if(!$this->img->saveAs($this->getImagePath() . $this->img->name))
                    return false;
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $this->saveAuthors($this->authorNames);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookAuthors()
    {
        return $this->hasMany(BookAuthor::className(), ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['id' => 'author_id'])->viaTable('book_author', ['book_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\BookQuery(get_called_class());
    }

    public function getImagePath(){
        return \Yii::getAlias('@statics').'/web/images/';
    }

    public function getImageSrc($image){
        if(!file_exists($this->getImagePath(). '/' . $image) || !$image){
            return \Yii::getAlias('@web').'/images/default.jpg';
        }
        $imageDir = Yii::$app->getAssetManager()->publish($this->getImagePath());
        $fileName = \Yii::$app->getAssetManager()->getPublishedUrl($imageDir[0]). '/' . $image;
        return $fileName;
    }

    public function bookAuthorsLinks(){
        $authors = $this->authors;
        $return = '';
        if($authors){
            foreach($authors as $author){
                $return .= $return ? ',<br>' : '';
                $return .= Html::a($author->getFullName(), Url::to(['author/view', 'id'=>$author->id]));
            }
        }
        return $return;
    }


    public function getAuthorNames(){
        if($this->_authorNames)
            return $this->_authorNames;

        if($this->authors){
            return ArrayHelper::map($this->authors, 'id', 'id');
        }
    }
    public function setAuthorNames($authors){
        $this->_authorNames = $authors;
    }

    public function saveAuthors($authorKeys){
        $relatedKeys = [];

        foreach ($this->authors as $model) {
            $relatedKeys[] = $model->getPrimaryKey();
        }

        // удаляем отсутствующие в $orderKeys связи
        foreach (array_diff($relatedKeys, $authorKeys) as $id) {
            $model = Author::findOne($id);
            $this->unlink('authors', $model, true);
        }

        // добавляем новые связи
        foreach (array_diff($authorKeys, $relatedKeys) as $id) {
            $model = Author::findOne($id);
            $this->link('authors', $model);
        }
    }

}
