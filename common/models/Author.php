<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "author".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $middlename
 *
 * @property BookAuthor[] $bookAuthors
 * @property Book[] $books
 */
class Author extends \yii\db\ActiveRecord
{

    /**
     * var string Полное имя автора
     * */
    private $_fullName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'author';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'name'], 'required'],
            [['surname', 'name', 'middlename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'middlename' => 'Отчество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookAuthors()
    {
        return $this->hasMany(BookAuthor::className(), ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])->viaTable('book_author', ['author_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AuthorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AuthorQuery(get_called_class());
    }


    public function getFullName(){
        return $this->surname.' '.$this->name. ($this->middlename ? ' '.$this->middlename : '');
    }
}
