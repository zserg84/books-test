<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 04.03.16
 * Time: 18:03
 */

namespace frontend\controllers;


use common\models\Author;
use frontend\models\form\AuthorForm;
use frontend\models\search\AuthorReportSearch;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use Yii;

class AuthorController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'report'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false,
                        'roles' => ['*']
                    ],
                ]
            ]
        ];
    }

    public function actionIndex(){
        $dataProvider = new ActiveDataProvider([
            'query' => Author::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionReport(){
        $model = new AuthorReportSearch();
        $dataProvider = $model->search(Yii::$app->getRequest()->get());
        return $this->render('report', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }


    public function actionView($id){
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate(){
        $model = new Author();

        if($model->load(\Yii::$app->getRequest()->post())){
            $this->ajaxValidate($model);
            if($model->save()){
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id){
        $model = $this->findModel($id);

        if($model->load(\Yii::$app->getRequest()->post())){
            $this->ajaxValidate($model);
            if($model->save()){
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id){
        $model = $this->findModel($id);
        if($model->delete()){
            return $this->redirect(['index']);
        }
        else{
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }

    public function findModel($id){
        return parent::_findModel(Author::className(), $id);
    }
} 