<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 05.03.16
 * Time: 11:47
 */

namespace frontend\controllers;


use common\models\Book;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;

class BookController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false,
                        'roles' => ['*']
                    ],
                ]
            ]
        ];
    }


    public function actionIndex(){
        $dataProvider = new ActiveDataProvider([
            'query' => Book::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($id){
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate(){
        $model = new Book();

        if($model->load(\Yii::$app->getRequest()->post())){
            $model->img = UploadedFile::getInstance($model, 'image');

            $this->ajaxValidate($model);
            if($model->save()){
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id){
        $model = $this->findModel($id);

        if($model->load(\Yii::$app->getRequest()->post())){
            $model->img = UploadedFile::getInstance($model, 'image');

            $this->ajaxValidate($model);
            if($model->save()){
                return $this->redirect(['view', 'id'=>$model->id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id){
        $model = $this->findModel($id);
        if($model->delete()){
            return $this->redirect(['index']);
        }
        else{
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }

    public function findModel($id){
        return parent::_findModel(Book::className(), $id);
    }
} 