<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 04.03.16
 * Time: 18:03
 */

namespace frontend\controllers;

use Yii;
use yii\base\Exception;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class FrontendController extends Controller
{

    protected function _findModel($modelName, $id){
        $model = $modelName::findOne($id);
        if(!$model){
            throw new Exception('Unknown model');
        }
        return $model;
    }

    public function ajaxValidate($model){
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
} 