<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 05.03.16
 * Time: 12:10
 */

use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title = 'Книга '.$model->title;
$this->params['breadcrumbs'] = [
    $this->title
];

$attributes = [
    'title', 'year', 'isbn',
    [
        'attribute'=>'authorNames',
        'format'=>'raw',
        'value' => $model->bookAuthorsLinks(),
    ],
    [
        'attribute'=>'image',
        'value'=>$model->getImageSrc($model->image),
        'format' => ['image',['width'=>'100','height'=>'100']],
    ]

];

echo DetailView::widget([
    'model'=>$model,
    'attributes'=>$attributes,
]);