<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Author;

?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'book_form',
        'enctype' => 'multipart/form-data',
    ]
]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'authorNames')->dropDownList(ArrayHelper::map(Author::find()->all(), 'id', 'fullName'), [
                'multiple' => true
            ])?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'title')?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'year')?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'description')->textarea()?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'isbn')?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'image')->fileInput()?>
        </div>
    </div>

<?= Html::submitButton('Сохранить',
    [
        'class' => $model->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php ActiveForm::end(); ?>