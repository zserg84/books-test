<?php
use backend\widgets\spaceImage\SpaceImageWidget;
use backend\web\theme\widgets\Box;

$this->title = 'Книга '.$model->title;
$this->params['breadcrumbs'] = [
    $this->title
];
?>
<div class="row">
    <div class="col-sm-12">
        <?php
        echo $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );?>
    </div>
</div>