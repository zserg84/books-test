<?php

$this->title = 'Книги';
$this->params['subtitle'] = 'Создание';
$this->params['breadcrumbs'] = [
    $this->title,
]; ?>
<div class="row">
    <div class="col-sm-12">
        <?
        echo $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );?>
    </div>
</div>