<?php

$this->title = 'Авторы';
$this->params['subtitle'] = 'Создание';
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
]; ?>
<div class="row">
    <div class="col-sm-12">
        <?
        echo $this->render(
            '_form',
            [
                'model' => $model,
            ]
        );?>
    </div>
</div>