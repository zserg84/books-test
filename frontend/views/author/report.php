<?
use yii\grid\GridView;
use yii\grid\CheckboxColumn;
use yii\bootstrap\Html;

$gridId = 'author-list';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'attribute'=>'surname',
            'format'=>'raw',
            'value' => function($data){
                return Html::a($data->surname, ['author/view', 'id'=>$data->id]);
            },
        ],
        'name', 'middlename',
        [
            'attribute' => 'booksCnt',
        ],
    ]
];
?>

<div class="row">
    <div class="col-xs-4">
        <?= $this->render('_reportForm', [
            'model' => $model,
        ]);?>
    </div>
    <div class="col-xs-12">
        <?=  GridView::widget($gridConfig);?>
    </div>
</div>