<?
use yii\grid\GridView;
use yii\grid\CheckboxColumn;
use yii\grid\ActionColumn;
use yii\bootstrap\Html;
use yii\helpers\Url;

$gridId = 'book-list';

$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        'surname', 'name', 'middlename',
    ]
];
if(!Yii::$app->user->isGuest){
    $actions[] = '{update}';
    $actions[] = '{delete}';
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions),
    ];
}
?>
<?if(!Yii::$app->user->isGuest){
    echo Html::a('Создать', Url::to(['author/create']),
        [
            'class' => 'btn btn-primary btn-large'
        ]
    );
} ?>
<div class="row">
    <div class="col-xs-12">
        <?=  GridView::widget($gridConfig);?>
    </div>
</div>