<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'author_form',
    ],
    'method' => 'get'
]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'year')?>
        </div>
    </div>

<?= Html::submitButton('Найти',
    [
        'class' => 'btn btn-success btn-large'
    ]
) ?>
<?php ActiveForm::end(); ?>