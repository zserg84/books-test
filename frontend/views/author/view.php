<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 05.03.16
 * Time: 12:10
 */

use yii\widgets\DetailView;

$this->title = 'Автор '.$model->getFullName();
$this->params['breadcrumbs'] = [
    $this->title
];

$attributes = [
    'surname', 'name', 'middlename'
];

echo DetailView::widget([
    'model'=>$model,
    'attributes'=>$attributes,
]);