<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 05.03.16
 * Time: 15:12
 */

namespace frontend\models\search;

use common\models\Author;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class AuthorReportSearch extends Author
{
    /**
     * @var integer Год выпуска
     * */
    public $year;
    /**
     * @var integer Количество книг автора
     * */
    public $booksCnt;

    public $limit = 10;

    public function rules(){
        return [
            [['year'], 'number']
        ];
    }

    public function attributeLabels(){
        return array_merge(parent::attributeLabels(),
            [
                'year' => 'Год выпуска',
                'booksCnt' => 'Количество книг'
            ]
        );
    }

    public function search($params = null)
    {
        if (!($this->load($params) && $this->validate())) {
            $this->year = date('Y');
        }

        $query = self::authorsListQuery($this->year);
        $query = $query->limit($this->limit);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        return $dataProvider;
    }

    /**
     * @return \common\models\query\AuthorQuery Список авторов с макс. количеством книг.
     */
    public static function authorsListQuery($year){
        $query = self::find()->select('author.*, COUNT(DISTINCT book.id) as booksCnt')->innerJoinWith([
            'books' => function($query) use($year){
                $query->year($year);
            }
        ])->groupBy('author.id')->orderBy('booksCnt desc');

        return $query;
    }
} 