<?php
/**
 * Created by PhpStorm.
 * User: sz
 * Date: 05.03.16
 * Time: 11:54
 */

namespace frontend\models\form;

use common\models\Book;

class BookForm extends Book
{
    public $img;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['img'], 'file', 'mimeTypes'=> ['image/png', 'image/jpeg', 'image/gif'], 'wrongMimeType' => 'Недопустимый тип файла'],
        ]);
    }
} 