<?php

use yii\db\Migration;

class m160304_083551_author extends Migration
{
    public function up()
    {
        $this->createTable('author', [
            'id' => $this->primaryKey(),
            'surname' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'middlename' => $this->string(),
        ]);

        $this->createTable('book_author', [
            'book_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('pk_book_author', 'book_author', 'book_id, author_id');
        $this->addForeignKey('fk_book_author_book_id__book_id', 'book_author', 'book_id', 'book', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_book_author_author_id__author_id', 'book_author', 'author_id', 'author', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('book_author');
        $this->dropTable('author');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
