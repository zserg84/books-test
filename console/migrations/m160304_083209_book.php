<?php

use yii\db\Migration;

class m160304_083209_book extends Migration
{
    public function up()
    {
        $this->createTable('book', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'year' => $this->integer()->notNull(),
            'description' => $this->text(),
            'isbn' => $this->string()->notNull(),
            'image' => $this->string()
        ]);
        $this->createIndex('uidx_book_isbn', 'book', 'isbn', true);
    }

    public function down()
    {
        $this->dropTable('book');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
